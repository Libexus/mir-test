"use strict";

function download(filename, string) {
    let elem = document.createElement("a");
    elem.setAttribute("href", "data:text/plain;charset=utf-8,"
    + encodeURIComponent(string));
    elem.setAttribute("download", filename);
    elem.style.display = "none";

    document.body.appendChild(elem);
    elem.click();
    document.body.removeChild(elem);
}

function upload(callback) {
    let elem = document.createElement("input");
    elem.setAttribute("type", "file");
    elem.setAttribute("accept", ".json,application/json,text/plain");
    elem.style.display = "none";
    document.body.appendChild(elem);
    elem.click();
    elem.onchange = () => {
        let file = elem.files[0];
        let reader = new FileReader();
        reader.readAsText(file);
        reader.onload = (e) => {
            callback(reader.result);
            document.body.removeChild(elem);
        }
    }
}

function updateWord(wordData, index = undefined) {
    if ( index == undefined) {
        data.data.push(wordData);
    } else {
        data.data[index] = wordData;
    }
}

function saveWordlist(name, data) {
    localStorage.setItem("wordlist." + name, JSON.stringify(data));
    let availableLists = JSON.parse(localStorage.getItem("availableLists"));
    if ( availableLists == null ) {
        availableLists = [];
    }
    if (availableLists.indexOf(name) == -1) {
        availableLists.push(name);
    }
    localStorage.setItem("availableLists", JSON.stringify(availableLists));
}

function getWordlist(name) {
    let reg = /data\/.*\.json/;
    if(reg.test(name)) {
        return getDataHttp(name);
    } else {
        return JSON.parse(localStorage.getItem("wordlist." + name));
    }

}

function downloadWordlist(filename, listname) {
    let data = getWordlist(listname);
    if( data != null) {
        download(filename, JSON.stringify(data));
        return true;
    } else {
        return false;
    }
}

function queryWordlists() {
    let list = JSON.parse(localStorage.getItem("availableLists"));
    return list != null ? list : new Array();
}

function getDataHttp(jsonfile) {
    let xhttp = new XMLHttpRequest();
    let responseText;
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
          responseText = JSON.parse(this.responseText);
        }
    } 
    xhttp.open("GET", jsonfile, false); // Synchron -> warten
    xhttp.send();
    return responseText;
}

function material_icon(name) {
    let node = document.createElement("span");
    node.classList.add("material-icons");
    node.textContent = name;
    return node;
}