"use strict";
// (c) Libexus, 2020

let currentWordElement;
let wordChanged = false;

// Invoked at the end of file
function init() {
    document.getElementById("editor-grid").classList.add("hidden");
    rebuildWordlist(getDataHttp("data/nouns.json"));
    document.getElementById("editor-save").onclick = saveCurrentWord;
    document.getElementById("wordlist-new").onclick = newWord;
    document.getElementsByClassName("editor-title-left")[0]
        .addEventListener("keypress", (e) => {
            if(e.keyCode == 13) {
                e.preventDefault();
            }
        });
}

function displayFormByType(wordType) {
    let editor_grid = document.getElementById("editor-grid");
    editor_grid.innerHTML = "";

    for(let id of inputIds[wordType]) {
        let elem = inputElements[id[0]];
        switch(elem[0]) {
            case 'i': {
                let label = document.createElement("label");
                label.setAttribute("for", id[0]);
                label.textContent = elem.substr(1);
                let input = document.createElement("input");
                input.setAttribute("type", "text");
                input.setAttribute("id", id[0]);
                input.classList.add("editor-input");
                editor_grid.appendChild(label);
                editor_grid.appendChild(input);
                break;
            }
            case 'l': {
                let title_elem = document.querySelector("#editor-title > .editor-title-left");
                title_elem.textContent = elem.substr(1);
                title_elem.id = id[0];
                title_elem.classList.add("editor-input");
                let type_elem = document.getElementById("editor-wordtype");
                type_elem.textContent = `(${wordTypeNames[wordType]})`;
                break;
            }
            case 'r': {
                let title_elem = document.querySelector("#editor-subtitle > input");
                let info_elem  = document.querySelector("#editor-subtitle > span");
                title_elem.id = id[0];
                title_elem.classList.add("editor-input");
                info_elem.textContent = elem.substr(1);
                break;
            }
            default:
                throw "Warnung: Falscher input-Typ!";
                break;
        }
    }
    editor_grid.classList.remove("hidden");

    for ( let elem of document.getElementsByClassName("editor-input")) {
        elem.addEventListener("keypress", (event) => {
            wordChanged = true;
            if ( event.keyCode == 13) { // Enter
                saveCurrentWord();
            } else {
                currentWordElement.classList.add("changed");
            }
        });
    }
}

function displayFormByWord(wordbox) {
    let word = wordbox.wordData;
    displayFormByType(word.gr[0]);
    let first = true;
    for (let id of inputIds[word.gr[0]]) {
        let elem = document.getElementById(id[0]);
        if(elem.nodeName == "INPUT") {
            elem.value = word[id[1]] != undefined ? word[id[1]] : "";
            if(first) { elem.focus(); elem.select(); first = false; }
        } else {
            if ( word[id[1]] != undefined ) {
                elem.textContent = word[id[1]];
            }
            if(first) { elem.focus(); window.getSelection().selectAllChildren(elem); first = false; }
        }
    }
    currentWordElement = wordbox;
}

function extractFormData() {
    let word = currentWordElement.wordData;
    let wordData = {}; // Entry of data[]
    for (let id of inputIds[word.gr[0]]) {
        let elem = document.getElementById(id[0]);
        if(elem.nodeName == "INPUT") {
            word[id[1]] = elem.value;
        } else {
            word[id[1]] = elem.textContent;
        }
    }
    return word;
}

function finaliseWord(word) {
    let gr = "";
    switch (word.gr[0]) {
        case "n":
            gr = word.gr[0] + word._sexus[0] + word._dkl[0];
            break; 
        case "v": 
            gr = word.gr[0] + word._konj[0];
            break;
        case "j": 
            gr = word.gr[0] + word._dkl[0];
            break;
        case "p": 
            gr = word.gr[0] + word._dkl[0];
            break;
        case "k":
        case "a":
        case "r":
        case "x": break;
        default :  throw `"${word.gr[0]}" ist keine gültige Wortart!`;
    }
    word.gr = gr;

    let finalWord = {};
    let keys = Object.keys(word);
    for ( let key of keys ) {
        if( key[0] != "_" ) {
            finalWord[key] = word[key];
        }
    }
    return finalWord;
}

function extractWordlist() {
    let wordlistElement = document.getElementById("wordlist-list");
    let data = { data: [] };
    for (let elem of wordlistElement.children) {
        data.data.push(finaliseWord(elem.wordData));
    }
    return data;
}

function rebuildWordlist(data) {
    document.getElementById("editor-grid").classList.add("hidden");
    let wordlistElement = document.getElementById("wordlist-list");
    // TODO: Diese Schleife sollte wegen Performance erst ausgeführt werden, wenn die zukünftigen Elemente schon erstellt sind.
    while ( wordlistElement.firstElementChild ) {
        wordlistElement.removeChild(wordlistElement.lastElementChild);
    }
    data.data.forEach( (word, i, arr) => {
        wordlistElement.appendChild(createWordbox(word));
    });
}

function createWordbox(word) {
    let preview;
    switch (word.gr[0]) {
        case "n":
            preview = word.sg1;
            word._sexus = word.gr[1];
            word._dkl = word.gr[2];
            break; 
        case "v": 
            preview = word.inf;
            word._konj = word.gr[1];
            break;
        case "j": 
            preview = word.sg1;
            word._dkl = word.gr[1];
            break;
        case "p": 
            preview = word.dekl.m[0] + '/' 
                + word.dekl.f[0] + '/' 
                + word.dekl.n[0];
            word._dekl = word.gr[1];
            break;
        case "k":
        case "a":
        case "r":
        case "x": preview = word.lat; break;
        default :  throw `"${word.gr[0]}" ist keine gültige Wortart!`;
    }
    let wordbox = document.createElement("div");
    wordbox.classList.add("wordbox");
    wordbox["wordData"] = word;

    let previewElement = document.createElement("span");
    previewElement.addEventListener("click", () => displayFormByWord(wordbox) );
    previewElement.appendChild(document.createTextNode(preview));
    previewElement.classList.add("wordbox-word");

    let deleteElement = document.createElement("span");
    deleteElement.innerHTML = "X";
    deleteElement.classList.add("wordbox-close");
    deleteElement.onclick = (e) => {
        if ( confirm("Wort löschen?") ) {
            if(e.currentTarget.parentElement == currentWordElement) {
                document.getElementById("editor-grid").classList.add("hidden");
            }
            wordbox.parentElement.removeChild(wordbox);
        }
    };

    wordbox.appendChild(previewElement);
    wordbox.appendChild(deleteElement);

    return wordbox;
}

function saveCurrentWord() {
    updateCurrentWordElement(createWordbox(finaliseWord(extractFormData())));
    wordChanged = false;
    currentWordElement.classList.remove("changed");
}

function updateCurrentWordElement(elem) {
    if(currentWordElement != null && currentWordElement.parentNode != null) {
        currentWordElement.parentNode.replaceChild(elem, currentWordElement);
    } else {
        document.getElementById("wordlist-list").appendChild(elem);
    }
    currentWordElement = elem;
}

function newWord() {
    if ( wordChanged && confirm("Aktuelles Wort speichern?") == true ) {
        saveCurrentWord();
    }
    currentWordElement = undefined;
    try {
        let type = prompt("Wortart:");
        if (type != undefined) {
            let box = createWordbox({gr: type});
            updateCurrentWordElement(box);
            displayFormByWord(box);
        }
    } catch (err) {
        alert("Fehler: " + err.toString());
        throw err;
    }
}

/**
 * Enthält IDs nach Wortart. Zuerst die HTML-id, dann den Names des Feldes in der Wörterdatei
 * -> siehe format.md
 * 
 * '_id': Kein Feld in Datei erstellen
 */
const inputIds = {
    v: [["editor_inf", "inf"], ["editor_konj", "_konj"], ["editor_st","st"], ["editor_perf","p_st"], ["editor_ppp","ppp_st"], ["editor_tr","tr"]],
    n: [["editor_sg1","sg1"], ["editor_sex", "_sexus"], ["editor_dkl", "_dkl"], ["editor_st","st"], ["editor_tr","tr"]],
    j: [["editor_sg1","sg1"], ["editor_dkl2", "_dkl"], ["editor_st","st"], ["editor_tr","tr"]],
    a: [["editor_lat","lat"], ["editor_tr","tr"]],
    k: [["editor_lat","lat"], ["editor_tr","tr"]],
    r: [["editor_lat","lat"], ["editor_tr","tr"]],
    p: [["editor_dekl","dekl"],["editor_tr","tr"]],
};

/**
 * Arten von Inputs:
 * - 'i': <input>-Element in grid
 * - 'l' bzw. 'r': Linke bzw. rechte Überschrift
 * 
 * Aufbau:
 * editor_id: '<input type><label>',
 */
const inputElements = {
    editor_st: 'iStamm',
    editor_perf: 'iPerfekt',
    editor_ppp: 'iPPP',
    editor_custom_type: 'iEigene Wortart',
    editor_tr: 'iÜbersetzung',
    editor_lat: 'lLatein',
    editor_inf: 'lInfinitiv',
    editor_sg1: 'l1.F Sg',
    editor_konj:'r-Konj',
    editor_dkl: 'iDeklination',
    editor_dkl2: 'rDeklination',
    editor_sex: 'rGeschlecht',
};

const wordTypeNames = {
    v: 'Verb',
    n: 'Nomen',
    j: 'Adjektiv',
    a: 'Adverb',
    k: 'Konjunktion',
    r: 'Präposition',
    p: 'Pronomen',

}

init();