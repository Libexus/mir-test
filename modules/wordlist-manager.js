// Libexus, 2021

function newElement(type, clazz, appendTo) {
    let node = document.createElement(type);
	if( /^ | {2,}| $/.test(clazz) ) { throw new SyntaxError(`Fehler bei Klassenauswahl: "${clazz}"`); }
	for ( let c of clazz.split(" ") ) {
		node.classList.add("wordlist-manager-" + c);
	}
    if(appendTo) appendTo.appendChild(node);
    return node;
}

function blockEnter(event) {
	if(event.keyCode == 13) {
		event.preventDefault();
	}
}

export class WordlistManager {
    constructor(type) {
        this._rootNode = newElement("div", "root");
        this._rootNode.innerHTML = "<h1>Wortlisten-Manager</h1>";

        this._mainColumn = newElement("div", "maincolumn", this._rootNode);

        this._listColumn = newElement("div", "managercolumn", this._mainColumn);
        this._infoColumn = newElement("div", "infocolumn", this._mainColumn);

        this._listselector = new WordlistSelector();
        this._listColumn.appendChild(this._listselector.render());

        this._listinfo = new WordlistInfo(this._listselector);
        this._infoColumn.appendChild(this._listinfo.render());
        this._listselector.onEntrySelect = (event, entry) => {
            this._listinfo.display(entry.src, entry.wordlist);
        };
        this._listselector.onEntryDeselect = (event, entry) => {
            let selected = this._listselector.getSelected();
            if(selected.length > 1) {
                for ( let e of selected) {
                    if (e != entry.src) {
                        this._listinfo.display(e, this._listselector.querySelected()[e]);
                        break;
                    }
                }
            } else {
                this._listinfo.display();
            }
        }

        this._actionsRow = newElement("div", "actionsrow", this._rootNode);
        this._buttonCancel = newElement("button", "action-button", this._actionsRow);
        this._buttonUpload = newElement("button", "action-button", this._actionsRow);
        this._buttonNew = newElement("button", "action-button", this._actionsRow);
        this._buttonOpen = newElement("button", "action-button", this._actionsRow);

        this._buttonUpload.textContent = "Hochladen";
        this._buttonUpload.addEventListener("click", (event) => this._upload(event));

        this._buttonNew.textContent = "Neu";
        this._buttonNew.addEventListener("click", (event) => this.newWordlist());

        this._buttonCancel.textContent = "Abbrechen";
        this.oncancel = (event, mgr) => {};
        this._buttonCancel.addEventListener("click", (event) => this._oncancelhandler(event));

        this._buttonOpen.textContent = "Öffnen";
		this.onopen = (event, manager) => {};
        this._buttonOpen.addEventListener("click", (event) => this._onopenhandler(event));

		this._flags={};
		this.defaultFlags = {upload: true, new: true, cancel: true, open: false, editinfo: true};
		this.type = type;
    }

	set type(t) {
		switch(t) {
			case "open":    this.setOptions({upload: true, new: true,  open: true,  editinfo: false}); break;
			case "manager": this.setOptions({upload: true,  new: true,  open: false, editinfo: true }); break;
			default:        this.setOptions(this.defaultFlags);
		}
	}

	changeOptions(flags={}) {
		this._flags = Object.assign(this._flags, flags);
		this.setOptions(this._flags)
	}

	setOptions(flags) {
		this._flags = flags;
		this._buttonUpload.style.display = this._flags.upload ? "block" : "none";
		this._buttonNew.style.display    = this._flags.new    ? "block" : "none";
		//this._buttonCancel.style.display = this._flags.cancel ? "block" : "none";
		this._buttonOpen.style.display   = this._flags.open   ? "block" : "none";
		this._listinfo.allowEditing = this._flags.editinfo;
	}

	resetOptions() { this._flags = this.defaultFlags; }

	getSelected() {
		return this._listselector.getSelected();
	}

	querySelected() {
		return this._listselector.querySelected();
	}

    _upload(event) {
        upload((raw_data) => {
            let data = JSON.parse(raw_data);
            this.newWordlist(data);
        });
    }

	close() {
		this._rootNode.remove();
	}

	_onopenhandler(event) {
		let ret = this.onopen(event, this);
		if(ret || ret === undefined) {
			this.close();
		}
	}

	_oncancelhandler(event) {
		let ret = this.oncancel(event, this);
		if(ret || ret === undefined) {
			this.close();
		}
	}

    newWordlist(data) {
		let name = prompt("Dateiname:");
		if(name) {
			name = name.trim();
			if( this._listselector.availableLists.indexOf(name) != -1 ) {
				alert("Liste bereits vorhanden");
			} else if (/^ *$/.test(name)) {
				alert("Kein Name angegeben");
			} else {
				if(!data) {
					data = { name: name };
				}
				saveWordlist(name, data);
				this._listselector.rebuildList();
				this._listinfo.display(name, data);
			}
		}
    }
    
    render() {
        return this._rootNode;
    }
}

class WordlistSelector {
    constructor() {
        this._rootNode = newElement("div", "selector-root");
        this.onEntrySelect = () => {};
        this.onEntryDeselect = () => {};
		this._entries = [];
        this.rebuildList();
    }
    
    rebuildList() {
		let selected = this.getSelected();
        let availableLists = queryWordlists();
        this.availableLists = availableLists.concat(getDataHttp("data/index.json"));
        while(this._rootNode.firstElementChild) {
            this._rootNode.removeChild(this._rootNode.lastElementChild);
        }
        this._entries = [];
        for ( let list of this.availableLists ) {
			// Damit sich die Vorschau nicht ändert, werden die Events erst nachdem
			// die zuvor ausgewählten Einträge aktiviert wurden, hinzugefügt.
            let newEntry = new WordlistEntry(list);
			if(selected.indexOf(list) !== -1) {
				newEntry.select();
			}
            newEntry.onselect = (event, entry) => {
				if(!event.ctrlKey) {
					this.selectNone();
				}
				this.onEntrySelect(event, entry);
				entry.select();
			};
			newEntry.ondeselect = (event, entry) => {
				if(!event.ctrlKey && this.getSelected().length > 1) {
					this.selectNone();
					this.onEntrySelect(event, entry);
					entry.select();
				} else {
					this.onEntryDeselect(event, entry);
					entry.unselect();
				}
			}
            this._rootNode.appendChild(newEntry.render());
            this._entries.push(newEntry);
        }
    }

    getSelected() {
        let selected = [];
        for ( let e of this._entries ) {
            if(e.selected) {
                selected.push(e.src);
            }
        }
        return selected;
    }

    querySelected () {
        let selected = {};
        for ( let e of this._entries ) {
            if(e.selected) {
                selected[e.src] = e.wordlist;
            }
        }
        return selected;

    }

    selectNone() {
        for ( let e of this._entries ) {
            e.selected = false;
        }
    }

    selectAll() {
        for ( let e of this._entries ) {
            e.selected = true;
        }
    }

    render() {
        return this._rootNode;
    }
}

class WordlistEntry {
    constructor(wordlistsrc, onselect=()=>{}, ondeselect=()=>{}) {
        this._rootNode = newElement("div", "entry-root");
        let clickarea =  newElement("div", "entry-info", this._rootNode);
        this._nameNode = newElement("div", "entry-name", clickarea);
        this._idNode =   newElement("div", "entry-id", clickarea);
        this._downloadNode = newElement("div", "entry-icon", this._rootNode);
        this._deleteNode =   newElement("div", "entry-icon", this._rootNode);

        this._downloadNode.appendChild(material_icon("file_download"));
        this._deleteNode.appendChild(material_icon("clear"));

        this._nameNode.textContent = wordlistsrc;
        this._src = wordlistsrc;

        /* Regex:
         * (?:.*\/)?    Alles bis zum letzten Slash (falls vorhanden)
         * (.+?)        Dateiname ohne .json
         * (?:\.json|$) .json oder Zeilenende($)
         * (?:...)      Nichterfassende Gruppe
        */
        this._downloadNode.addEventListener("click", () => {
            let filename = this._src.match(/(?:.*\/)*(.+?)(?:\.json|$)/)[1] + ".json";
            download(filename, JSON.stringify(this.wordlist));
        })

        this.onselect = onselect;
        this.ondeselect = ondeselect;
        clickarea.addEventListener("click", (event) => {
            if(this._selected) {
                this.ondeselect(event, this);
            } else {
                this.onselect(event, this);
            }
        });

        this._wordlist = undefined;
        this._rootNode.addEventListener("mouseover", (event) => {
            if(!this._wordlist) {
                this._wordlist = getWordlist(wordlistsrc);
                this._idNode.textContent = this._wordlist.id;
            }
        });
    }
    get wordlist() { return this._wordlist }
    get src() { return this._src; }
    get selected() { return this._selected; }
    set selected(sel) {
        if (sel) { this._rootNode.classList.add("wordlist-manager-entry-selected"); }
        else {this._rootNode.classList.remove("wordlist-manager-entry-selected"); }
        this._selected = sel;
    }
    select()   { this.selected = true; }
    unselect() { this.selected = false; }
    toggle()   { this.selected = !this.selected; }

    render() { return this._rootNode; }
}

class WordlistInfo {
    constructor(wordlistselector) {
		this._wordlistselector = wordlistselector;

        this._rootNode = newElement("div", "info-very-root");
        this._infoRootNode = newElement("div", "info-root", this._rootNode);
        this._infoRootNode.classList.add("hidden"); 

        this._fileRootNode = newElement("div", "info-heading", this._infoRootNode);
		this._cancelBtn = newElement("div", "info-heading-btn info-heading-cancel", this._fileRootNode);
        this._fileNode = newElement("span", "info-heading-text", this._fileRootNode);
		this._editBtn = newElement("div", "info-heading-btn info-heading-edit-btn", this._fileRootNode);
		this._applyBtn = newElement("div", "info-heading-btn info-heading-apply", this._fileRootNode);

		this._cancelBtn.addEventListener("click", (event) => this.cancelEditing() );
		let cancelIcon = material_icon("cancel");
		this._cancelBtn.appendChild(cancelIcon);

		this._editBtn.addEventListener("click", (event) =>  this.startEditing() );
		let editNode = material_icon("edit")
		this._editBtn.appendChild(editNode);

		this._applyBtn.addEventListener("click", (event) => this.applyEditing() );
		let applyIcon = material_icon("done");
		this._applyBtn.appendChild(applyIcon);

		document.body.addEventListener("keydown", (event) => {
			if(event.keyCode == 27) {
				this.cancelEditing();
			}
		});

        this._nameNode = newElement("div", "info-entry", this._infoRootNode);
        let nameLabel = newElement("span", "info-entry-desc", this._nameNode);
        nameLabel.textContent = "Name:";
        this._nameNodeName = newElement("span", "info-entry-info info-entry-name", this._nameNode);
		this._nameNodeName.addEventListener("keypress", e => blockEnter(e));

        this._idNode = newElement("span", "info-entry", this._infoRootNode);
        let idLabel = newElement("span", "info-entry-desc", this._idNode);
        idLabel.textContent = "ID:";
        this._idNodeId = newElement("span", "info-entry-info info-entry-id", this._idNode);
		this._idNodeId.addEventListener("keypress", e => blockEnter(e));
        
        this._descNode = newElement("div", "info-entry info-entry-info info-entry-listdesc", this._infoRootNode);
		this._descNode.addEventListener("keypress", e => blockEnter(e));

        this._previewRootNode = newElement("div", "info-entry", this._infoRootNode);
        let previewLabel = newElement("span", "info-entry-desc", this._previewRootNode);
        previewLabel.textContent = "Vorschau:";
        this._previewNode = newElement("div", "info-preview", this._previewRootNode);

        this._nothingSelectedNode = newElement("div", "info-noselected", this._rootNode);
        let content = newElement("div", "info-noselected-content", this._nothingSelectedNode);
        let text1 = newElement("div", "info-noselected-heading", content);
        let text2 = newElement("div", "info-noselected-text", content);
        
        text1.textContent = "Kein Wort ausgewählt";
        text2.textContent = "Nix!";

		this._currentFileName = undefined;
		this._currentWordlist = undefined;
    }

    display(filename, wordlist) {
		this._currentFileName = filename;
		this._currentWordlist = wordlist;
        if( !filename && !wordlist) {
            this._infoRootNode.classList.add("hidden");
        } else {
            this._infoRootNode.classList.remove("hidden");
            this._nameNodeName.textContent = wordlist.name;
            this._fileNode.textContent = filename;
            this._idNodeId.textContent = wordlist.id;
            this._descNode.textContent = wordlist.desc != undefined ? wordlist.desc : "Keine Beschreibung angegeben";
            this._previewNode.textContent = "TODO: Vorschau";
        }
    }

	set allowEditing(allow) {
		this._allowEditing = allow;
		this._editBtn.style.visibility = allow ? "visible" : "hidden";
	}

	startEditing() {
		this._infoRootNode.classList.add("edit");
		for( let e of document.getElementsByClassName("wordlist-manager-info-entry-info")) {
			e.contentEditable = true;
		}
	}

	cancelEditing() {
		// TODO: Prompt
		this._infoRootNode.classList.remove("edit");
		for( let e of document.getElementsByClassName("wordlist-manager-info-entry-info")) {
			e.contentEditable = false;
		}
		this.display(this._currentFileName, this._currentWordlist);
	}

	applyEditing() {
		// TODO: Prompt
		this._infoRootNode.classList.remove("edit");
		for( let e of document.getElementsByClassName("wordlist-manager-info-entry-info")) {
			e.contentEditable = false;
		}
		this._currentWordlist.name = this._nameNodeName.textContent;
		this._currentWordlist.data = this._nameNodeName.textContent;
		this._currentWordlist.id   = this._idNodeId.textContent;
		this._currentWordlist.desc = this._descNode.textContent;
		saveWordlist(this._currentFileName, this._currentWordlist);
		this._wordlistselector.rebuildList();
	}

    render() {
        return this._rootNode;
    }
}
